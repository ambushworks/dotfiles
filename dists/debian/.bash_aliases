alias python=python3
alias pip=pip3

alias bt=bashtop

alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias lh='ls -lah'

# info
alias myip='curl http://ipecho.net/plain; echo'
alias td='date && cal'
sysinfo () {
    myip
    uname -a
    return
}

# file system
alias projects='cd ~/Projects && ls'
alias df='df -h'
ffc () {
    find . -name *$1*
}

# git
alias glp='git log --pretty=oneline --abbrev-commit'
alias gst='git status'


# environment
alias pe='printenv | less'

# system helpers
alias alsa-reload='sudo alsa force-reload'
alias rs='pulseaudio --kill; sleep 2s; sudo alsa force-reload ; pulseaudio --start'

# example bash function
mcd () {
    mkdir -p $1
    cd $1
}

# Docker
alias dub='docker-compose up --build'
dew () {
    docker exec -it app_web_1 sh
}

# Docker Machine Aliases
alias dmls='docker-machine ls'
dmip () {
    docker-machine ip $1
}

alias vpn='sudo openvpn --config ~/.vpn/pf-UDP4-1194-lloyd-android-config.ovpn'

# ytd="youtube-dl --add-metadata -ic"
# ytda="youtube-dl --add-metadata -xic" # Download audio only
