export PATH="$PATH:/home/lloyd/snap/bin"

# Docker Machine shell prompt
# PS1='[\u@\h \W$(__docker_machine_ps1)]\$ '

export LD_LIBRARY_PATH=/usr/local/cuda-11.1/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
source <(kubectl completion bash)

export ANDROID_SDK_ROOT=/home/lloyd/Android/Sdk
#export ANDROID_AVD_HOME=/home/lloyd/.android/avd