#!/bin/bash

#email=""

sudo apt update && sudo apt upgrade -y
sudo apt install unattended-upgrades apt-listchanges bsd-mailx
sudo dpkg-reconfigure -plow unattended-upgrades


# Configure unatended upgrades
file="/etc/apt/apt.conf.d/50unattended-upgrades"

# Enable auto reboot
line='\/\/Unattended-Upgrade::Automatic-Reboot \"false\";'
replace='Unattended-Upgrade::Automatic-Reboot \"true\";'
sudo sed -i "s/$line/$replace/" $file

#line='//Unattended-Upgrade::Mail \"\";'
#replace='Unattended-Upgrade::Mail \"$email\";'
#sudo sed -i "s/$line/$replace/" $file

#edit the /etc/apt/listchanges.conf and set email ID:
#email_address=vivek@server1.cyberciti.biz

sudo unattended-upgrades --dry-run
