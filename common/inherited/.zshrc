# zsh History configuration
HISTSIZE=10000
SAVEHIST=10000

# Import secrets file
if [ -f ~/.shell_secrets ]; then
  source ~/.shell_secrets
fi

if [[ -f ~/.zsh_aliases ]]; then
  source ~/.zsh_aliases
fi

if [[ -f ~/.dev_aliases ]]; then
  source ~/.dev_aliases
fi


### Development ###

# Kubernetes configuration
if [ -x "$(command -v helm)" ]; then
  source <(helm completion zsh)
fi
if [ -x "$(command -v kubectl)" ]; then
  source <(kubectl completion zsh)
fi
if [ -x "$(command -v minikube)" ]; then
  eval $(minikube docker-env)
fi

# Add flutterfire to path
if [ -d "$HOME/.pub-cache/bin" ] ; then
    PATH="$HOME/.pub-cache/bin:$PATH"
fi

# Load Angular CLI autocompletion.
if command -v ng &> /dev/null; then
    # Load Angular CLI autocompletion.
    source <(ng completion script)
fi


if [ -d "$HOME/.development/anaconda3" ] ; then
  # Add anaconda to path
  PATH="$HOME/.development/anaconda3/bin:$PATH"

  # >>> conda initialize >>>
  # !! Contents within this block are managed by 'conda init' !!
  __conda_setup="$('$HOME/.development/anaconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
  if [ $? -eq 0 ]; then
      eval "$__conda_setup"
  else
      if [ -f "$HOME/.development/anaconda3/etc/profile.d/conda.sh" ]; then
          . "$HOME/.development/anaconda3/etc/profile.d/conda.sh"
      else
          export PATH="$HOME/.development/anaconda3/bin:$PATH"
      fi
  fi
  unset __conda_setup
  # <<< conda initialize <<<
fi

### Binaries ###

# Set Nodejs Path
NODEVERSION=v20.11.0
NODEDISTRO=linux-x64
export PATH="$HOME/.local/lib/nodejs/node-$NODEVERSION-$NODEDISTRO/bin:$PATH"
export PATH="$HOME/.development/flutter/bin:$PATH"
export PATH="$HOME/.development/android-studio/bin:$PATH"

### Plugins ###
