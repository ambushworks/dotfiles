#!/bin/bash

dist=""
if [[ -f /etc/arch-release ]]; then
	dist="arch"
elif [[ -f /etc/debian_version ]]; then
	dist="debian"
else
	# Distro not identified
	exit 1
fi

echo "Running $dist based distro."

# cycle common and distribution directories looking for files
declare -a dirsarr=("$PWD/common" "$PWD/dists/$dist")
for dir in "${dirsarr[@]}"; do
	for file in $(find $dir -maxdepth 1 -name ".*" -type f  -printf "%f\n" ); do
	    # only backup if the original file exists and isn't a symlink
	    if [ -e ~/$file ] && [ ! -L ~/$file ]; then
	        mv -f ~/$file{,.dtbak}
	    fi

	    if [ ! -e ~/$file ]; then
		 # create simlink if file doesn't exist in home
	        ln -s $dir/$file ~/$file
	    fi
	done
done


if [[ $dist == "debian" ]]; then
	# Create a blank .bash_secrets file
	if [ ! -f ~/.bash_secrets ]; then
		touch ~/.bash_secrets
	fi
	
	# install packages
	apt update
	#apt install vim zsh zsh-syntax-highlighting autojump
	apt install vim zsh autojump
	
	# set vim as default
	update-alternatives --set editor /usr/bin/vim.basic

	# Setup zsh
	mkdir -p ~/.zsh
	git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.zsh/powerlevel10k
	git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.zsh/zsh-syntax-highlighting
	  
	echo "Set zsh as default shell"
	chsh -s $(which zsh)
	exec zsh
elif [[ $dist == "arch" ]]; then
	sudo pacman -Syu
	sudo pacman -S --needed vim curl docker docker-compose

	sudo sed -Ei '/EnableAUR/s/^#//' /etc/pamac.conf
	sudo pamac install autojump

	# configure docker to run without sudo
	if ! getent group docker > /dev/null; then
    	sudo groupadd docker
    fi
	sudo usermod -aG docker $USER

	# activate group changes without logout
	newgrp docker

	# if cant reach docker socket without sudo
	# https://unix.stackexchange.com/questions/252684/why-am-i-getting-cannot-connect-to-the-docker-daemon-when-the-daemon-is-runnin
	# sudo chgrp docker /usr/bin/docker
	# sudo chgrp docker /var/run/docker.sock
	# sudo systemctl restart docker
	
	# Workstation Applications
	# sudo pacman -S code brave-browser nextcloud-client obsidian onlyoffice guake
fi

# Make plugin directory for vim
mkdir -p ~/.vim/plug/autoload

# Install vim plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
echo "run :PlugInstall in vim to install plugins"
echo "Finished"
